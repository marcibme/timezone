using System.Collections.Generic;

namespace TimeZonesApp.API.Model
{
    public class User
    {
        public User()
        {
            this.FavoriteTimeZones = new List<FavouriteTimeZone>();
        }
        public int Id { get; set; }

        public string Username { get; set; }

        public string PasswordHash { get; set; }

        public virtual IList<FavouriteTimeZone> FavoriteTimeZones { get; set; }
    }
}