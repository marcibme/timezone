using System.Collections.Generic;

namespace TimeZonesApp.API.Model
{
    public class TimeZone
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int UtcValue { get; set; }

    }
}