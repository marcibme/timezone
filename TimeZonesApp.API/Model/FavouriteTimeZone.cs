namespace TimeZonesApp.API.Model
{
    public class FavouriteTimeZone
    {
        public int UserId { get; set; }
        public User User { get; set; }

        public int TimeZoneId { get; set; }
        public TimeZone TimeZone { get; set; }
    }
    
}