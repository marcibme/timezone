using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TimeZonesApp.API.Model;

namespace TimeZonesApp.API.Data
{
    public class TimeZoneRepository : ITimeZoneRepository
    {
        private readonly DataContext _context;
        public TimeZoneRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<List<TimeZone>> GetAllTimeZone()
        {
            return await _context.TimeZones.ToListAsync();
        }

        public async Task<TimeZone> GetTimeZone(int timeZoneId)
        {
            return await _context.TimeZones.SingleOrDefaultAsync(t => t.Id == timeZoneId);
        }

        public Task<TimeZone> GetServerTimeZone()
        {
            throw new System.NotImplementedException();
        }

        public async Task<List<TimeZone>> GetUsersFavouriteTimeZones(int userId)
        {
            return await _context.FavouriteTimeZones.Where(f => f.UserId == userId).Select(f => f.TimeZone).ToListAsync();
        }

        public async Task<FavouriteTimeZone> GetFavouriteTimeZone(int timezoneId, int userId)
        {
            return await _context.FavouriteTimeZones.Where(f => f.UserId == userId && f.TimeZoneId == timezoneId).SingleOrDefaultAsync();
        }

        public void AddFavouriteTimeZone(FavouriteTimeZone favTimeZone)
        {
            _context.FavouriteTimeZones.Add(favTimeZone);
        }

        public async Task<bool> SaveChanges()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}