using System.Threading.Tasks;
using TimeZonesApp.API.Model;

namespace TimeZonesApp.API.Data
{
    public interface IAuthRepository
    {
         Task<User> Login(string username, string password);
    }
}