using Microsoft.EntityFrameworkCore;
using TimeZonesApp.API.Model;

namespace TimeZonesApp.API.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base (options) {}

        public DbSet<TimeZone> TimeZones { get; set; }                 
        public DbSet<User> Users { get; set; }                 
        public DbSet<FavouriteTimeZone> FavouriteTimeZones { get; set; }                 
        
        protected override void  OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FavouriteTimeZone>().ToTable("FavouriteTimeZones").HasKey(ft => new { ft.UserId, ft.TimeZoneId });

            modelBuilder.Entity<User>().HasData(new User {Username = "seed", PasswordHash = "password", Id = 1});
            modelBuilder.Entity<User>().HasData(new User {Username = "teszt", PasswordHash = "password", Id = 2});

            modelBuilder.Entity<TimeZone>().HasData(new TimeZone {Name = "Utc-0", UtcValue = 0, Id = 1});
            modelBuilder.Entity<TimeZone>().HasData(new TimeZone {Name = "Utc-1", UtcValue = -1, Id = 2});
            modelBuilder.Entity<TimeZone>().HasData(new TimeZone {Name = "Utc-2", UtcValue = -2, Id = 3});
            modelBuilder.Entity<TimeZone>().HasData(new TimeZone {Name = "Utc+1", UtcValue = 1, Id = 4});
            modelBuilder.Entity<TimeZone>().HasData(new TimeZone {Name = "Utc+2", UtcValue = 2, Id = 5});


            base.OnModelCreating(modelBuilder);
        }
    }
}