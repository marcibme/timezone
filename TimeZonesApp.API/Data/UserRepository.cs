using System.Threading.Tasks;
using TimeZonesApp.API.Model;

namespace TimeZonesApp.API.Data
{
    public class UserRepository : IUserRepository
    {
        private readonly DataContext _context;
        public UserRepository(DataContext context)
        {
            _context = context;
        }
        public async Task<User> GetUser(int userId)
        {
            return await _context.Users.FindAsync(userId);
        }
    }
}