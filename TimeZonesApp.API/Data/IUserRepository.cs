using System.Threading.Tasks;
using TimeZonesApp.API.Model;

namespace TimeZonesApp.API.Data
{
    public interface IUserRepository
    {
         Task<User> GetUser(int userId);
         
    }
}