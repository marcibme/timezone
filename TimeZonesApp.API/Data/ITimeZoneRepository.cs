using System.Collections.Generic;
using System.Threading.Tasks;
using TimeZonesApp.API.Model;

namespace TimeZonesApp.API.Data
{
    public interface ITimeZoneRepository
    {
         Task<TimeZone> GetServerTimeZone();
         Task<List<TimeZone>> GetAllTimeZone();
         Task<TimeZone> GetTimeZone(int timeZoneId);
         Task<List<TimeZone>> GetUsersFavouriteTimeZones(int userId);
         Task<FavouriteTimeZone> GetFavouriteTimeZone(int timezoneId, int userId);

         void AddFavouriteTimeZone(FavouriteTimeZone favTimeZone);
         Task<bool> SaveChanges();
         
    }
}