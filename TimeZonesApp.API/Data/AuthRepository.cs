using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TimeZonesApp.API.Model;

namespace TimeZonesApp.API.Data
{
    public class AuthRepository : IAuthRepository
    {
        private readonly DataContext _context;
        public AuthRepository(DataContext context)
        {
            _context = context;
        }
        public async Task<User> Login(string username, string password)
        {
            var user = await _context.Users.SingleOrDefaultAsync(u => u.Username == username);

            if(user == null)
                return null;

            if(VerifyPasswordHash(password, user.PasswordHash))
            return user;

            return null;
        }

        private bool VerifyPasswordHash(string password, string passwordHash)
        {
            
                if(password.Equals(passwordHash))
                    return true;
                return false;
            
        }
    }
}