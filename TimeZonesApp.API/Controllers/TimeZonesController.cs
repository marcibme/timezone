﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TimeZonesApp.API.Data;
using TimeZonesApp.API.Model;

namespace TimeZonesApp.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TimeZonesController : ControllerBase
    {
        private readonly ITimeZoneRepository _timezoneRepo;
        private readonly IUserRepository _userRepo;

        public TimeZonesController(ITimeZoneRepository timezoneRepo, IUserRepository userRepo)
        {
            _timezoneRepo = timezoneRepo;
            _userRepo = userRepo;
        }

        // GET api/timezones
        [HttpGet]
        public async Task<IActionResult> GetTimeZones()
        {
            var timeZones = await _timezoneRepo.GetAllTimeZone();

            return Ok(timeZones);
        }

        // GET api/timezones/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTimeZone(int id)
        {
            var timeZone = await _timezoneRepo.GetTimeZone(id);

            return Ok(timeZone);
        }


        [HttpPost("{timezoneId}/user/{userId}")]
        public async Task<IActionResult> AddFavouriteTimeZone(int timezoneId, int userId)
        {

            var favourite = await _timezoneRepo.GetFavouriteTimeZone(timezoneId, userId);

            if (favourite != null)
                return BadRequest("You already added this to favourites");
            
            var user = await _userRepo.GetUser(userId);
            if (user == null)
                return NotFound();

            var timezone = await _timezoneRepo.GetTimeZone(timezoneId);
            if(timezone == null)
                return NotFound();


            favourite = new FavouriteTimeZone
            {
                UserId = user.Id,
                TimeZoneId = timezone.Id
            };

            _timezoneRepo.AddFavouriteTimeZone(favourite);

            if (await _timezoneRepo.SaveChanges())
                return Ok();
            
            return BadRequest("Failed to add to favourites");
        }
    }
}
