﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TimeZonesApp.API.Migrations
{
    public partial class SeedTimezones : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "TimeZones",
                columns: new[] { "Id", "Name", "UtcValue" },
                values: new object[] { 1, "Utc-0", 0 });

            migrationBuilder.InsertData(
                table: "TimeZones",
                columns: new[] { "Id", "Name", "UtcValue" },
                values: new object[] { 2, "Utc-1", -1 });

            migrationBuilder.InsertData(
                table: "TimeZones",
                columns: new[] { "Id", "Name", "UtcValue" },
                values: new object[] { 3, "Utc-2", -2 });

            migrationBuilder.InsertData(
                table: "TimeZones",
                columns: new[] { "Id", "Name", "UtcValue" },
                values: new object[] { 4, "Utc+1", 1 });

            migrationBuilder.InsertData(
                table: "TimeZones",
                columns: new[] { "Id", "Name", "UtcValue" },
                values: new object[] { 5, "Utc+2", 2 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "TimeZones",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "TimeZones",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "TimeZones",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "TimeZones",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "TimeZones",
                keyColumn: "Id",
                keyValue: 5);
        }
    }
}
