﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TimeZonesApp.API.Migrations
{
    public partial class FavouriteTimeZoneTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FavouriteTimeZones",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    TimeZoneId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FavouriteTimeZones", x => new { x.UserId, x.TimeZoneId });
                    table.ForeignKey(
                        name: "FK_FavouriteTimeZones_TimeZones_TimeZoneId",
                        column: x => x.TimeZoneId,
                        principalTable: "TimeZones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FavouriteTimeZones_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FavouriteTimeZones_TimeZoneId",
                table: "FavouriteTimeZones",
                column: "TimeZoneId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FavouriteTimeZones");
        }
    }
}
