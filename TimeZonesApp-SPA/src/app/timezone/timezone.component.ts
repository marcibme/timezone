import { TimezoneService } from './../_services/timezone.service';
import { TimeZone } from './../_model/TimeZone';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-timezone',
  templateUrl: './timezone.component.html',
  styleUrls: ['./timezone.component.css']
})
export class TimezoneComponent implements OnInit {
timezones: TimeZone[];

  constructor(private timeZoneService: TimezoneService) { }

  ngOnInit() {
    this.loadTimeZones();
  }


  loadTimeZones() {
    this.timeZoneService.getAllTimeZone().subscribe((timezones: TimeZone[]) => {
      this.timezones = timezones;
    }, error => {
      console.log(error);
    });
  }
}
