import { TimezoneService } from './../../_services/timezone.service';
import { Component, OnInit, Input } from '@angular/core';
import { TimeZone } from 'src/app/_model/TimeZone';
import { AuthService } from 'src/app/_services/auth.service';

@Component({
  selector: 'app-timezone-row',
  templateUrl: './timezone-row.component.html',
  styleUrls: ['./timezone-row.component.css']
})
export class TimezoneRowComponent implements OnInit {
@Input() timezone: TimeZone;

  constructor(private timeZoneService: TimezoneService, private authService: AuthService) { }

  ngOnInit() {
  }

  addToFavourites(id: number) {
    this.timeZoneService.addToFavourites(id, this.authService.getCurrentUserId()).subscribe(data => {
      console.log('Added to favourites!');
    }, error => {
      console.log(error);
    });
  }
}
