export interface FavouriteTimeZone {
    userId: number;
    timeZoneId: number;
}
