export interface TimeZone {
    id: number;
    name: string;
    utcValue: number;
}
