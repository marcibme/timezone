import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { TimeZone } from '../_model/TimeZone';


@Injectable({
  providedIn: 'root'
})
export class TimezoneService {

  constructor(private http: HttpClient) { }
  baseUrl = environment.apiUrl;

  getAllTimeZone(): Observable<TimeZone[]> {
      return this.http.get<TimeZone[]>(this.baseUrl + 'timezones');
  }

  getTimeZone(id): Observable<TimeZone> {
    return this.http.get<TimeZone>(this.baseUrl + 'timezones/' + id);
  }

  addToFavourites(timezoneId: number, userId: number) {
    return this.http.post(this.baseUrl + 'timezones/' + timezoneId + '/user/' + userId, {});
  }
}
